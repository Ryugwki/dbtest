﻿using System.ComponentModel.DataAnnotations;

namespace Test.Models
{
    public class Accounts
    {
        [Key]
        public int AccountID { get; set; }
        [Required]
        public int CustomerID { get; set; }
        public Customer? Customer { get; set; }
        [Required, StringLength(100)]
        public string AccountName { get; set; }
    }
}
