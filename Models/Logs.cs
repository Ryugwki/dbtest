﻿using System.ComponentModel.DataAnnotations;

namespace Test.Models
{
    public class Logs
    {
        public int LogsID { get; set; }
        public int TransactionalID { get; set; }
        public Transactions? Transactions { get; set; }
        public DateOnly LoginDate { get; set; }
        public TimeOnly LoginTime { get; set; }
    }
}
