﻿using System.ComponentModel.DataAnnotations;

namespace Test.Models
{
    public class Transactions
    {
        [Key]
        public int TransactionalID { get; set; }
        [Required]
        public int CustomerID { get; set; }
        public Customer? Customer { get; set; }
        [Required]
        public int EmployeeID { get; set; }
        public Employees? Employees { get; set; }
    }
}
