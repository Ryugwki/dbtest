﻿using System.ComponentModel.DataAnnotations;


namespace Test.Models
{
    public class Customer
    {
        public int CustomerID { get; set; }
        [Required, StringLength(100)]
        public string FirstName { get; set; }
        [Required, StringLength(100)]
        public string LastName { get; set; }
        public string ContactAndAddress { get; set; }
    }
}
