﻿using System.ComponentModel.DataAnnotations;

namespace Test.Models
{
    public class Reports
    {
        [Key]
        public int ReportID { get; set; }
        [Required]
        public int AccountID { get; set; }
        public Accounts? Accounts { get; set; }
        [Required]
        public int LogsID { get; set; }
        public Logs? Logs { get; set; }
        [Required]
        public int TransactionalID { get; set; }
        public Transactions? Transactions { get; set; }
        public string ReportName { get; set; }
        public DateTime ReportDate { get; set; }
    }
}
